//
//  WACForecast.swift
//  Weather
//
//  Created by Ryan on 28/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation

class WACForecast {
    var temperature : NSInteger!    // Degrees celcius
    var weather : String!           // Description string
    var wind : NSInteger!           // Wind speed in KPH
    var date : NSDate!
    
    // Failable initializer
    public init?(json: [String: Any]) {
        
        // Set the date
        let date = json["dt"] as? Double
        
        // Set the temperature
        let mainDict = json["main"] as! [String: Any]
        let temperature = mainDict["temp"] as? NSInteger
        
        // Set the wind
        let windDict = json["wind"] as! [String: Any]
        let wind = windDict["speed"] as? NSInteger
        
        // Set the weather
        let weatherArray = json["weather"] as! [Any]
        let weatherDict = weatherArray.first as! [String: Any]
        let weather = weatherDict["main"] as? String
        
        // Convert from Kelvin into degrees (to an rough accuracy of 0 d.p)
        self.temperature = temperature! - 273
        // Remove the quotation marks
        self.weather = weather?.replacingOccurrences(of: "\"", with: "")
        self.wind = wind
        self.date = NSDate(timeIntervalSince1970: date!)
        
        // Return nil if we do not have all attributes
        if (temperature != nil && weather != nil && wind != nil && date != nil) {
            self.temperature = temperature! - 273
            self.weather = weather
            self.wind = wind
            self.date = NSDate(timeIntervalSince1970: date!)
        } else {
            return nil
        }
    }
    
    static func getJSONArrayFromResponseData(jsonData : Data?) -> [[String:Any]] {
        var dictionaryArray = [[String:Any]]()
        
        do {
            let jsonResults = try JSONSerialization.jsonObject(with: jsonData!, options: []) as! [String:Any]
            if (jsonResults["list"] != nil) {
                dictionaryArray = jsonResults["list"] as! [[String:Any]]
            }
        } catch let error {
            // Something has gone wrong with our deserialization, print the error out
            print(error.localizedDescription)
        }
        return dictionaryArray
    }
    
    static func takeJSONArrayAndCreateArrayOfForecasts(jsonResults : [[String:Any]]) -> [WACForecast] {
        var forecastArray = [WACForecast]()
        // Loop through all jsonResults and for every one, create a city object and add it to array
        for object in jsonResults {
            if let forecast = WACForecast(json: object) {
                forecastArray.append(forecast)
            }
        }
        return forecastArray
    }
}
