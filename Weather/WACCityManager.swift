//
//  WACCityManager.swift
//  Weather
//
//  Created by Ryan on 28/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation

class WACSingletonManager {
    static let sharedInstance = WACSingletonManager()
    // Global city array
    var cityArray : [WACCity]!
    // Global selected city
    var selectedCity : WACCity!
}
