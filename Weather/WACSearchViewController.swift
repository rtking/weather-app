//
//  WACSearchViewController.swift
//  Weather
//
//  Created by Ryan on 28/03/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class WACSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // Outlets
    @IBOutlet weak var searchController : UISearchBar!
    @IBOutlet weak var searchTableView : UITableView!
    
    var locations = [WACCity]()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: SEARCH METHODS
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if WACSingletonManager.sharedInstance.cityArray != nil {
            self.locations = WACSingletonManager.sharedInstance.cityArray.filter{ $0.name.contains(searchText) }
            self.searchTableView.reloadData()
        }
    }
    
    // MARK: UITABLEVIEW METHODS
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(WACConstants.tableViewCellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Instantiate the custom cell
        let cell  = self.searchTableView.dequeueReusableCell(withIdentifier: "searchCell")!
        
        
        // Get the forecast out of the array
        let location = self.locations[indexPath.row]

        cell.textLabel?.text = location.name
        cell.detailTextLabel?.text = location.country
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Get the forecast out of the array
        WACSingletonManager.sharedInstance.selectedCity = self.locations[indexPath.row]
    }
}

