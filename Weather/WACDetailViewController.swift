//
//  ViewController.swift
//  Weather
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class WACDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // Outlets
    @IBOutlet var mapView : MKMapView!
    @IBOutlet var weatherTableView : UITableView!
    
    // Variables
    var forecasts = [WACForecast]()
    
    // MARK: VIEW LIFECYCLE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register custom UITableView class
        self.weatherTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        // Set up map view
        self.setupMapView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Send network call
        self.sendNetworkCall()
    }
    
    // MARK: NETWORK METHODS
    
    func sendNetworkCall() {
        // Ensure our selected city is not nil
        if WACSingletonManager.sharedInstance.selectedCity != nil {
            // Use HTTPClient to get us the forecast data
            WACHTTPClient.getFiveDayWeatherForecastForCity(cityID: WACSingletonManager.sharedInstance.selectedCity.id!) { (data) in
                // Function returns a list of JSON dictionaries
                let jsonResults = WACForecast.getJSONArrayFromResponseData(jsonData: data!)
                // Create array of forecasts
                self.forecasts = WACForecast.takeJSONArrayAndCreateArrayOfForecasts(jsonResults: jsonResults) 
                
                // Reload our table in the main thread, as UIKit is not thread safe
                DispatchQueue.main.async {
                    self.weatherTableView.reloadData()
                }
            }
        } else {
            // Create alert controller to inform the user of invalid city
            let alert = UIAlertController(title: "Invalid City", message: "Sorry we are unable to find your specified city, please try again", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: MAP METHODS
    
    func setupMapView() {
        if let city = WACSingletonManager.sharedInstance.selectedCity {
            if (city.longitude != nil && city.latitude != nil) {
                let location = CLLocationCoordinate2DMake(city.latitude, city.longitude)
                self.mapView.setRegion(MKCoordinateRegionMakeWithDistance(location, WACConstants.cityViewSpan, WACConstants.cityViewSpan), animated: true)
            }
        }
    }
    
    // MARK: UITABLEVIEW METHODS
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(WACConstants.tableViewCellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecasts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Instantiate the custom cell
        let cell : WACWeatherTableViewCell = self.weatherTableView.dequeueReusableCell(withIdentifier: "weatherCell") as! WACWeatherTableViewCell
        
        // Get the forecast out of the array
        let forecast = self.forecasts[indexPath.row]
        
        // Set the labels to correct values
        cell.weatherDescriptionLabel.text  = "\(forecast.weather!)"
        cell.temperatureDescriptionLabel.text = "\(forecast.temperature!)° C, \(forecast.wind!) kph"
        
        // Format our date to a readable string
        let myFormatter = DateFormatter()
        myFormatter.dateStyle = .short
        cell.dayLabel.text = myFormatter.string(from: forecast.date! as Date)
        
        // Format our time to a readable string
        myFormatter.dateFormat = "HH:mm"
        cell.timeLabel.text = myFormatter.string(from: forecast.date! as Date)
        
        // Set our image view
        switch forecast.weather! {
            case "Snow":
                cell.weatherImageView.image = #imageLiteral(resourceName: "snow")
            case "Rain":
                cell.weatherImageView.image = #imageLiteral(resourceName: "rain")
            case "Clouds":
                cell.weatherImageView.image = #imageLiteral(resourceName: "clouds")
            case "Clear":
                cell.weatherImageView.image = #imageLiteral(resourceName: "clear")
            default:
                cell.weatherImageView.image = nil
        }
        
        return cell
    }

    // MARK: MEMORY MANAGEMENT

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

