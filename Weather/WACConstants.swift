//
//  WACConstants.swift
//  Weather
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit
import Foundation

class WACConstants {
    // API constants
    static let openWeatherMapAPI = "5830b4cbbc4dbf80d54f8969d0863baa"
    static let openWeatherAPIBaseURL = "http://api.openweathermap.org/data/2.5/forecast"
    
    // UI constants
    static let tableViewCellHeight = 64.0
    static let cityViewSpan = 10000.0
}
