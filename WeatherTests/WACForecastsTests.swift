//
//  WACParsingResponseForecastTests.swift
//  Weather
//
//  Created by Ryan on 28/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import XCTest

@testable import Weather

class WACForecastsTests: XCTestCase {
    
    var jsonData : Data!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // Setup the JSON test file
        if let path = Bundle.main.path(forResource: "weathertest", ofType: "json") {
            do {
                // Turn the JSON into data
                self.jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
            } catch let error {
                // Something has gone wrong with our deserialization, print the error out
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testParsingResponseJSON() {
        // Ensure that the test data is not nil
        if self.jsonData != nil {
            // Function returns a list of JSON dictionaries
            let jsonResults = WACForecast.getJSONArrayFromResponseData(jsonData: self.jsonData)
            // Create array of forecasts
            let forecastArray = WACForecast.takeJSONArrayAndCreateArrayOfForecasts(jsonResults: jsonResults)
            // Check that we have all 40 weather forecasts
            XCTAssertEqual(forecastArray.count, 40)
            
            let forecastAtIndexEleven = forecastArray[11]
            // Check that our temperature is correct
            XCTAssertEqual(forecastAtIndexEleven.temperature!, 22)
            // Check that our weather is correct
            XCTAssertEqual(forecastAtIndexEleven.weather!, "Clouds")
            // Check that our date is correct
            XCTAssertEqual(forecastAtIndexEleven.wind!, 4)
        } else {
            print("JSON Data is nil, sorry")
        }
    }
}
