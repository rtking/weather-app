//
//  WACNetworkCallsTests.swift
//  Weather
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import XCTest

@testable import Weather

class WACNetworkCallsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // Hardcoded to cityID 2193734, Auckland NZ
    func testNetworkCallForWeatherGivenCityID() {
        // Create expectation
        let expected = expectation(description: "API Call Complete")
        
        // Make network call for the five day forecast of Auckland
        WACHTTPClient.getFiveDayWeatherForecastForCity(cityID: 2193734) { (data) in
            // Complete expectation
            expected.fulfill()
        }
        
        // Wait 10 seconds for expectation to be completed
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
