//
//  WACParsingLocalCitiesTests.swift
//  Weather
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import XCTest

@testable import Weather

class WACCitiesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testParsingLocalCityJSON() {
        // Function returns a list of JSON dictionaries
        let jsonResults = WACCity.getJSONArrayFromLocalCityData()
        // Check that we have all 209579 jsonResults in the list
        XCTAssertEqual(jsonResults.count, 209579)
        
        let cityArray = WACCity.takeJSONArrayAndCreateArrayOfCities(jsonResults: jsonResults)
        // Check that we have all 209579 cities in the list
        XCTAssertEqual(cityArray.count, 209579)
        
        let cityAtIndexEight = cityArray[8]
        // Check that our city has correct id
        XCTAssertEqual(cityAtIndexEight.id, 1283240)
        // Check that our city has correct name
        XCTAssertEqual(cityAtIndexEight.name, "Kathmandu")
        // Check that our city has correct country
        XCTAssertEqual(cityAtIndexEight.country, "NP")
    }
    
    // Hardcoded to Auckland NZ
    func testFilteringReturnsCorrectCity() {
        // Create some cities
        let auckland = WACCity(json: ["_id" : 2193734,
                                      "name" : "Auckland",
                                      "country" : "NZ",
                                      "coord" : [ "lon" : 174.783325, "lat" : -36.8499985]]);
        let london = WACCity(json: ["_id" : 2643743,
                                      "name" : "London",
                                      "country" : "GB",
                                      "coord" : [ "lon" : -0.12574, "lat" : 51.50853]]);
        let tokyo = WACCity(json: ["_id" : 1850147,
                                      "name" : "Tokyo",
                                      "country" : "JP",
                                      "coord" : [ "lon" : 139.691711, "lat" : 35.689499]]);
        
        // Put them into test array
        let cityArray = [auckland!, london!, tokyo!]
        
        // Test retrieving a valid city
        var retrievedCity = WACCity.findLocationInArray(cityArray: cityArray, location: "Auckland")
        // Check that our city has correct id
        XCTAssertEqual(retrievedCity?.id, 2193734)
        // Check that our city has correct name
        XCTAssertEqual(retrievedCity?.name, "Auckland")
        // Check that our city has correct country
        XCTAssertEqual(retrievedCity?.country, "NZ")
        // Check that our city has correct longitude
        XCTAssertEqual(retrievedCity?.longitude, 174.783325)
        // Check that our city has correct latitude
        XCTAssertEqual(retrievedCity?.latitude, -36.8499985)
        
        // Test retrieving an invalid city
        retrievedCity = WACCity.findLocationInArray(cityArray: cityArray, location: "Neptune")
        // Check that our city is nil
        XCTAssertNil(retrievedCity)
    }
}
